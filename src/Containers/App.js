import React, { Component } from 'react';
import './App.css';
import Burger from '../Components/Burger/Burger';
import Buttons from '../Components/Buttons/Buttons';

class App extends Component {
  state = {
    ingredients: [],
  };
  ingredientsMenu = [
    {filling:"Salad", price:5},
    {filling:"Cheese", price:20},
    {filling:"Meat", price:50},
    {filling:"Bacon", price:30}
  ];

  addIngredient = (key ) => {
    const ingredients = [...this.state.ingredients];
    ingredients.push(key);
    console.log(ingredients);
    this.setState({ingredients});
  }
  remuveIngredient = (key) => {
    const ingredients = [...this.state.ingredients];
    const index = ingredients.findIndex((p) => p === key);
    if (index !== -1) {
      ingredients.splice(index, 1);
      this.setState({ingredients});
    }
  }
  blockBtn = (item) =>{
    let isBlock = true;
    this.state.ingredients.forEach((key) => {
      if (key === item) {
        isBlock = false;
        return isBlock;
      }
    })
    return isBlock;
  }

  render() {
    return (
      <div className="App">
      <Burger
       ingredients={this.state.ingredients}
        />
      <Buttons
      ingredients={this.state.ingredients}
       block={this.blockBtn}
       remuve={this.remuveIngredient}
       add={this.addIngredient}
       menu={this.ingredientsMenu}
       />
      </div>
    );
  }
}

export default App;
