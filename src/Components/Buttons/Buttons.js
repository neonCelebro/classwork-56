import React, {} from 'react';
import './Buttons.css';
import Button from './Button/Button.js';

const totalPrice = (props) => {
  let totalPrice = 20;
  props.ingredients.forEach((key) => {
    totalPrice = totalPrice + key.price;
  })
  return <span>И того к оплате: {totalPrice} com</span>
}

const Buttons = (props) => {
  return(
    <div className='buttonsContainer'>
    {props.menu.map((item, index) =>
      <Button
       name={item.filling}
       block={props.block(item)}
       remuve={() => props.remuve(item)}
       add={() => props.add(item)}
       key={index} />)}
       {totalPrice(props)}
    </div>
  )
}

export default Buttons;
